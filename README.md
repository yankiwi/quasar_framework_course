### Quasar Framework Course

---

```
[X] Section 1: Intro
[X] Section 2: Setting Started
[X] Section 3: Vue.js Basics
[X] Section 4: Vue.js Lists and Components
[X] Section 5: Start Building Awesome Todo App
[X] Section 6: Create List of Tasks
[X] Section 7: Vuex - State Management
[X] Section 8: Vuex - Actions & Mutations
[X] Section 9: Add Task - Forms, Fields, Validation
[X] Section 10: Edit Task - Child Components in-depth, Update and Sync, Slots
[X] Section 11: Split Tasks into "Todo" and "Completed" Sections
[X] Section 12: Add a Search Bar
[X] Section 13: Add a Sort Dropdown (to sort Tasks by Name / Due Date)
[X] Section 14: Improve the app with Transitions, Directives, Filters, Mixins & Scroll Area
[X] Section 15: Settings Page
[X] Section 16: Create a Login & Register Page
[ ] Section 17: Firebase - Introduction
[ ] Section 18: Firebase - Authentication
[ ] Section 19: Firebase - Setup the Data Structure
[ ] Section 20: Firebase - Reading Data
[ ] Section 21: Firebase - Writing Data
[ ] Section 22: Firebase - Improve The Loading Experience
[ ] Section 23: Firebase - Multiple Users & Database Rules
[ ] Section 24: Firebase - Handle Errors & Show Notifications
[ ] Section 25: Platforms - Web
[ ] Section 26: Platforms - Mac (Electron)
[ ] Section 27: Platforms - Windows (Electron)
[ ] Section 28: Platforms - iOS (Cordova)
[ ] Section 29: Platforms - Android (Cordova)
[ ] Section 30: Course Round Up
[ ] Section 31: Bonus
```
